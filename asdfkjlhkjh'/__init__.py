# import tkinter as tk
#
#
# class Gui(tk.Frame):
#     def __init__(self, root):
#         tk.Frame.__init__(self, root)
#         lbl1 = tk.Label(self, text='here', borderwidth=2, relief="ridge")
#
#         right = tk.Frame(self)
#         lbl2 = tk.Label(right, text='here', borderwidth=2, relief="ridge")
#         lbl3 = tk.Label(right, text='here', borderwidth=2, relief="ridge")
#         lbl2.grid(row=1)
#         lbl3.grid(row=2)
#
#         lbl1.grid(row=0, column=0)
#         right.grid(row=0, column=1)
#
#         tk.Grid.columnconfigure(self, 0, weight=1)
#         tk.Grid.rowconfigure(self, 0, weight=1)
#         tk.Grid.columnconfigure(self, 1, weight=1)
#         tk.Grid.rowconfigure(self, 1, weight=1)
#         tk.Grid.columnconfigure(self, 2, weight=1)
#         tk.Grid.rowconfigure(self, 2, weight=1)
#         lbl1.grid(row=0, column=0, sticky=tk.N + tk.S + tk.E + tk.W)
#         lbl1.grid(sticky=tk.N + tk.S + tk.E + tk.W, column=0, row=7, columnspan=2)
#
#
# root = tk.Tk()
# stuff = Gui(root)
# stuff.grid()
# root.mainloop()

# import tkinter as tk
# from tkinter import ttk
#
# root = tk.Tk()
# frame = tk.Frame(root)
# tk.Grid.rowconfigure(root, 0, weight=1)
# tk.Grid.columnconfigure(root, 0, weight=1)
# frame.grid(row=0, column=0, sticky=tk.N + tk.S + tk.E + tk.W)
# grid = tk.Frame(frame)
# grid.grid(sticky=tk.N + tk.S + tk.E + tk.W, column=0, row=7, columnspan=2)
# tk.Grid.rowconfigure(frame, 7, weight=1)
# tk.Grid.columnconfigure(frame, 0, weight=1)
#
# # example values
#
# # lbl1 = tk.Label(frame, text='here', borderwidth=2, relief="ridge")
# #
# # right = tk.Frame(frame, borderwidth=2, relief="ridge")
# # right.grid(row=0, column=0, sticky=tk.N + tk.S + tk.E + tk.W)
# # tk.Grid.rowconfigure(right, 0, weight=1)
# # tk.Grid.columnconfigure(right, 0, weight=1)
# #
# # lbl2 = tk.Label(right, text='here', borderwidth=2, relief="ridge")
# # lbl3 = tk.Label(right, text='here', borderwidth=2, relief="ridge")
# # lbl1.grid(row=0, column=0, sticky=tk.N + tk.S + tk.E + tk.W)
# #
# # lbl2.grid(row=0, column=0, sticky=tk.N + tk.S + tk.E + tk.W)
# # lbl3.grid(row=1, column=0, sticky=tk.N + tk.S + tk.E + tk.W)
# # right.grid(row=0, column=1, sticky=tk.N + tk.S + tk.E + tk.W)
#
# # right.grid(row=0, column=1)
# # for x in range(10):
# #     for y in range(5):
# #         btn = tk.Button(frame)
# #         btn.grid(column=x, row=y, sticky=tk.N + tk.S + tk.E + tk.W)
#
# tree = ttk.Treeview(frame)
# tree.grid(column=0, row=11, sticky=tk.N + tk.S + tk.E + tk.W)
# for x in range(100):
#     tree.insert('', index='end', text='folderName')
#
# for x in range(11):
#     tk.Grid.columnconfigure(frame, x, weight=1)
#     # tk.Grid.columnconfigure(right, x, weight=1)
#
# for y in range(5):
#     tk.Grid.rowconfigure(frame, y, weight=1)
#     # tk.Grid.rowconfigure(right, y, weight=1)
#
# root.mainloop()
#
# # import tkinter as tk
# # from tkinter.ttk import Treeview
# #
# # root = tk.Tk()
# #
# # f1 = tk.Frame(root)
# # f2 = tk.Frame(root)
# #
# # f1.grid(column=0, row=0, sticky="ns")
# # f2.grid(column=1, row=0, sticky="n")
# # root.rowconfigure(0, weight=1)
# #
# # Treeview(f1).pack(expand=True, fill='y')
# # tk.Button(f2, text="DAT BUTTON IS IN F2").pack()
# # tk.Button(f2, text="DAT BUTTON IS IN F2").pack()
# # tk.Button(f2, text="DAT BUTTON IS IN F2").pack()
# #
# # root.mainloop()



import tkinter as tk
from tkinter import ttk

class Main(tk.Frame):

    def __init__(self, master):
        self.master = master
        tk.Frame.__init__(self, self.master) # , bg="red")

        self.pack(fill='both', expand=True)

        self.create_widgets()

    def create_widgets(self):
        # terminal output
        self.terminal_tree = ttk.Treeview(self)
        self.terminal_tree.grid(row=2, column=0, columnspan=5, sticky=tk.NSEW)
        self.columnconfigure(0, weight=1) # column with treeview
        self.rowconfigure(2, weight=1) # row with treeview
        for num in range(100):
            self.terminal_tree.insert('', index='end', text=str(num))


root = tk.Tk()
Main(root)
root.mainloop()