# general GUI colors
backgroundColor = '#334353'
foreground = 'silver'
componentBackground = 'black'

# file types
videoTypes = ['mp4', 'mov', 'avi', 'webm']
musicTypes = ['mp3', 'wav', 'aiff', 'alac', 'flac', 'aac', 'wma', 'ogg']
mediaTypes = [*videoTypes, *musicTypes]
pictureTypes = ['jpeg', 'png', 'jpg', 'img']

# file locations
wallpaperLocation = 'C:\\Users\\mason\\AppData\\Roaming\\Microsoft\\Windows\\Themes\\TranscodedWallpaper'
currentImageLocation = "mason media player picture DELETE ME.png"
