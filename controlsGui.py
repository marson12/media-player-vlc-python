import tkinter as tk
from tkinter import ttk

import eyed3

import constants
from guiComponents import ConfiguredButton, ConfiguredFrame, ConfiguredLabel, ConfiguredCheckBox, ConfiguredScale
import scannerUtil


class ControlsGui(ConfiguredFrame):
    def __init__(self, root, manager, data, *args, **kwargs):
        ConfiguredFrame.__init__(self, root, *args, **kwargs)

        # Set Connection
        self.manager = manager
        self.data = data

        # configure GUI
        self.bind('<Configure>', self.onResize)
        self.grid_columnconfigure(1, weight=1)
        self.grid_rowconfigure(1, weight=1)
        self.grid_rowconfigure(2, weight=1)

        # # # Left Side
        leftFrame = ConfiguredFrame(self)

        # volume slider
        self.volumeSlider = tk.Scale(leftFrame, variable=self.data.getVolumeObject(), from_=100, to=0,
                                     orient=tk.VERTICAL,
                                     activebackground=constants.foreground,
                                     bg=constants.backgroundColor,
                                     fg=constants.foreground,
                                     showvalue=False,
                                     highlightbackground=constants.foreground,
                                     troughcolor='black')

        # Volume Display
        self.volumeLabel = ConfiguredLabel(leftFrame)

        # place Left
        self.volumeSlider.grid(row=0, column=0)
        self.volumeLabel.grid(row=1, column=0)
        leftFrame.grid(row=0, column=0)

        # # # Right
        rightFrame = ConfiguredFrame(self)
        # # ROW 0
        row0Frame = ConfiguredFrame(rightFrame)
        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=1)

        # Watch Time
        self.timeSlider = ConfiguredScale(row0Frame, variable=self.data.getTimeObject(),
                                          command=self.manager.changeTime,
                                          to=100000,
                                          orient=tk.HORIZONTAL,
                                          length=100000
                                          )
        self.watchedLabel = ConfiguredLabel(row0Frame, text='0')

        # # ROW 1
        row1Frame = ConfiguredFrame(rightFrame)
        # media position slider

        # # ROW 2
        row2Frame = ConfiguredFrame(rightFrame)

        # playlist controls
        playlistControlsFrame = ConfiguredFrame(row2Frame)
        self.pauseBtn = ConfiguredButton(playlistControlsFrame, text='Pause', command=self.manager.togglePlay)
        self.nextMedia = ConfiguredButton(playlistControlsFrame, text='Next', command=self.manager.playNext)
        self.previousMedia = ConfiguredButton(playlistControlsFrame, text='Previous', command=self.manager.playPrevious)

        # # ROW 3
        row3Frame = ConfiguredFrame(rightFrame)

        # Repeat Song
        repeatFrame = ConfiguredFrame(row3Frame)
        repeatLabel = ConfiguredLabel(repeatFrame, text='Repeat')
        repeatLabel.bind('<Button-1>', self.clickRepeatLabel)

        self.repeatBox = ConfiguredCheckBox(repeatFrame, variable=self.data.getIsRepeatObject())

        # toggle fullscreen
        self.fullscreenBtn = ConfiguredButton(row3Frame, text='Fullscreen', command=self.manager.toggleFullscreen)

        # # ROW 4
        row4Frame = ConfiguredFrame(rightFrame)

        # add picture, added depending on media type
        self.mp3PictureBtn = ConfiguredButton(row4Frame, text='Set Music Picture',
                                              command=self.addDirectoryPictureToMp3)

        # Use Wallpaper as Image
        WallpaperFrame = ConfiguredFrame(row4Frame)
        wallpaperLabel = ConfiguredLabel(WallpaperFrame, text='Use Wallpaper')
        wallpaperLabel.bind('<Button-1>', self.clickWallpaperLabel)

        self.wallpaperBox = ConfiguredCheckBox(WallpaperFrame, variable=self.data.getIsUseWallpaperObject(),
                                               command=self.manager.toggleWallpaper)

        # wallpaper to mp3.  put the current image as the mp3s main image
        self.wallpaperToMp3Btn = ConfiguredButton(row4Frame, text='Set as mp3 Picture', command=self.wallpaperToMp3)

        # # PLACE
        self.buttonPadX = self.buttonPadY = 5
        # Place ROW 0
        self.timeSlider.grid(row=0, column=0)
        self.watchedLabel.grid(row=0, column=1)
        row0Frame.grid(row=0, column=0)

        # Place ROW 1
        row1Frame.grid(row=1, column=0)

        # Place ROW 2
        self.previousMedia.grid(row=0, column=0, padx=self.buttonPadX, pady=self.buttonPadY)
        self.pauseBtn.grid(row=0, column=1, padx=self.buttonPadX, pady=self.buttonPadY)
        self.nextMedia.grid(row=0, column=2, padx=self.buttonPadX, pady=self.buttonPadY)
        playlistControlsFrame.grid(row=0, column=1, padx=self.buttonPadX, pady=self.buttonPadY)

        row2Frame.grid(row=2, column=0)

        # Place ROW 3
        repeatLabel.grid(row=0, column=0)
        self.repeatBox.grid(row=0, column=1)
        repeatFrame.grid(row=0, column=0, padx=self.buttonPadX, pady=self.buttonPadY)

        self.fullscreenBtn.grid(row=0, column=1, padx=self.buttonPadX, pady=self.buttonPadY)

        # initiation dependent on file type. done elsewhere
        # self.mp3Picture.grid(row=0, column=2)

        row3Frame.grid(row=3, column=0)

        # place ROW 4
        wallpaperLabel.grid(row=0, column=0)
        self.wallpaperBox.grid(row=0, column=1)
        WallpaperFrame.grid(row=0, column=0, padx=self.buttonPadX, pady=self.buttonPadY)
        # self.wallpaperToMp3Btn.grid(row=0, column=0) # placement determined by data.isUsewallpaper

        row4Frame.grid(row=4, column=0)

        # place Right
        rightFrame.grid(row=0, column=1, sticky=tk.N)
        # add controls
        self.grid()

    def setPauseButton(self):
        self.pauseBtn.changeWidgetText('Pause')

        self.data.setIsPause(False)

    def setUnPauseButton(self):
        self.pauseBtn.changeWidgetText('UnPause')
        self.data.setIsPause(True)

    def setFullScreen(self):
        self.fullscreenBtn.changeWidgetText('Minimize')

    def setMinimizeScreen(self):
        self.fullscreenBtn.changeWidgetText('Fullscreen')

    def setWatchedLabel(self, text):
        self.watchedLabel['text'] = text

    def setVolumeLabel(self, volume):
        text = "Volume: "
        if volume < 10:
            text += '  '
        if volume < 100:
            text += '  '
        text += str(volume) + '%'
        self.volumeLabel.config(text=text)

    def setMusicControls(self):
        if scannerUtil.isMusicPath(self.data.getMediaPath()):
            self.mp3PictureBtn.grid(row=0, column=1, padx=self.buttonPadX, pady=self.buttonPadY)

            if self.data.getIsHasPicture() and not self.data.getIsUsewallpaper():
                self.wallpaperToMp3Btn.grid_forget()
            else:
                self.wallpaperToMp3Btn.grid(row=0, column=2, padx=self.buttonPadX, pady=self.buttonPadY)

        else:
            self.mp3PictureBtn.grid_forget()
            self.wallpaperToMp3Btn.grid_forget()

    def onResize(self, *unusedEvent):
        self.timeSlider.configure(length=self.winfo_width() - 200)

    def clickRepeatLabel(self, *unusedEvent):
        self.data.setIsRepeat(not self.data.getIsRepeat())

    def clickWallpaperLabel(self, *unusedEvent):
        self.data.setIsUsewallpaper(not self.data.getIsUsewallpaper())
        self.manager.toggleWallpaper()

    def addPictureToMp3(self, picturePath):
        audiofile = eyed3.load(self.data.getMediaPath())
        if (audiofile.tag == None):
            audiofile.initTag()
        else:
            audiofile.tag.images.remove('')
        audiofile.tag.images.set(3, open(picturePath, 'rb').read(), 'image')
        audiofile.tag.save(version=eyed3.id3.ID3_V2_3)
        self.data.setIsHasPicture(True)

    def addDirectoryPictureToMp3(self):
        time = self.data.getTime()
        self.manager.pausePlayer()

        picturePath = tk.filedialog.askopenfilename()
        if scannerUtil.isPicture(picturePath):
            self.addPictureToMp3(picturePath)

        self.manager.play(self.data.getMediaPath())
        self.data.setTime(time)
        self.manager.changeTime()

    def wallpaperToMp3(self):
        picturePath = constants.wallpaperLocation
        self.addPictureToMp3(picturePath)

# FEB 13, 14 in the afternoon, 20, 21 in the afternoon
# wedding shower
