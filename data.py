import configparser
import json
import tkinter as tk

import scannerUtil


class Data:
    def __init__(self, tkinterRoot):

        # object used to save and load data.
        self._config = configparser.ConfigParser()

        # MAIN OBJECT
        self._player = {}

        # player settings
        self._isFullScreen = False
        self._isMuted = False
        self._volume = tk.IntVar(value=50)
        self._time = tk.IntVar(value=0)

        self._isRepeat = tk.BooleanVar(value=False)
        self._isUsewallpaper = tk.BooleanVar(value=False)
        self._isHasPicture = False

        # directory items
        self._directoryRoot = ''
        # the directory will be turned into a dictionary with the keys being the paths of the folders,
        # and the value of the path being a list containing all the songs in that folder.
        # look at the path key to find things like parentfolders, subfolders.
        # # self.directory is in the form of {'folderPath':[vid1, vid2, ...], 'otherFolder':[song12, song4, ..]}
        self._directory = {}
        self._currentDirectoryList = []
        self._playlist = []

        self._shuffleMethod = tk.StringVar(value='Order')
        self._mediaPath = scannerUtil.programArguments()
        self._previousMedias = []
        self._customPlaylistDirectory = {}
        self._isPause = False

        # for when things break, the error will be saved.
        self._error = ''

        self._savedDataOrDefault()

    def _savedDataOrDefault(self):
        triesLeft = 3  # if there is some error then the file is deleted. 3 tries so that the user doesnt lose something
        #                                                                                           unless they have to.
        while triesLeft > 0:
            try:
                triesLeft -= 1
                if scannerUtil.fileExists(scannerUtil.getDataFilePath()):
                    self._config.read(scannerUtil.getDataFilePath(), encoding="utf-16")
                    self._isMuted = self._config.getboolean('controller', 'isMuted')
                    self._volume.set(self._config.getint('controller', 'volume'))
                    self._isRepeat.set(self._config.getboolean('controller', 'isRepeat'))
                    self._isUsewallpaper.set(self._config.getboolean('controller', 'isUsewallpaper'))

                    self._shuffleMethod.set(self._config.get('directory', 'shuffleMethod'))
                    if self.getMediaPath():
                        self._directoryRoot = scannerUtil.getParentDirectory(self.getMediaPath())
                        self._directory = scannerUtil.createDirectory(self._directoryRoot)
                        self._currentDirectoryList = self._directory[self._directoryRoot]
                    else:
                        self._directoryRoot = self._config.get('directory', 'directoryRoot')
                        self._mediaPath = self._config.get('directory', 'mediaPath')
                        self._time.set(self._config.getint('controller', 'time'))
                        self._currentDirectoryList = json.loads(self._config.get('directory', 'currentDirectoryList'))
                        self._playlist = json.loads(self._config.get('directory', 'playlist'))

                    self._previousMedias = json.loads(self._config.get('directory', 'previousMedias'))
                    self._customPlaylistDirectory = json.loads(
                        self._config.get('directory', 'customPlaylistDirectory'))
                    triesLeft = 0
            except:
                # something is wrong with this file. it should be deleted.
                if triesLeft == 0:
                    scannerUtil.deleteDataFile()

    def _save(self):
        self._config['controller'] = {
            'isMuted': self._isMuted
            , 'volume': self.getVolume()
            , 'time': self.getTime()
            , 'isRepeat': self.getIsRepeat()
            , 'isUsewallpaper': self.getIsUsewallpaper()
        }
        self._config['directory'] = {
            'directoryRoot': self.getDirectoryRoot()
            , 'currentDirectoryList': json.dumps(self.getCurrentDirectoryList())
            , 'playlist': json.dumps(self.getPlaylist())
            , 'shuffleMethod': self.getshuffleMethod()
            , 'mediaPath': self.getMediaPath()
            , 'previousMedias': json.dumps(self._previousMedias)
            , 'customPlaylistDirectory': json.dumps(self.getCustomPlaylistDirectory())
        }
        self._config['error'] = {
            'error': self.getError()
        }
        try:
            with open(scannerUtil.getDataFilePath(), 'w', encoding="utf-16") as file:
                self._config.write(file)
        except:
            pass

    def setPlayer(self, player):
        self._player = player

    def getPlayer(self):
        return self._player

    def setIsFullScreen(self, isFullScreen):
        self._isFullScreen = isFullScreen

    def getIsFullScreen(self):
        return self._isFullScreen

    def setIsMuted(self, isMuted):
        self._isMuted = isMuted
        self._save()

    def getIsMuted(self):
        return self._isMuted

    # def setVolumeObject(self, volumeObj):
    #     self._volume = volumeObj

    def getVolumeObject(self):
        return self._volume

    def setVolume(self, volume):
        self._volume.set(volume)
        self._save()

    def getVolume(self):
        return self._volume.get()

    # def setTimeObject(self, timeObj):
    #     self._time = timeObj

    def getTimeObject(self):
        return self._time

    def setTime(self, time):
        self._time.set(time)
        self._save()

    def getTime(self):
        return self._time.get()

    # def setIsRepeatObject(self, isRepeatObj):
    #     self._isRepeat = isRepeatObj

    def getIsRepeatObject(self):
        return self._isRepeat

    def setIsRepeat(self, isRepeat):
        self._isRepeat.set(isRepeat)
        self._save()

    def getIsRepeat(self):
        return self._isRepeat.get()

    # def setIsUsewallpaperObject(self, isUseWallpaperObj):
    #     self._isUsewallpaper = isUseWallpaperObj

    def getIsUseWallpaperObject(self):
        return self._isUsewallpaper

    def setIsUsewallpaper(self, isUsewallpaper):
        self._isUsewallpaper.set(isUsewallpaper)
        self._save()

    def getIsUsewallpaper(self):
        return self._isUsewallpaper.get()

    def setIsHasPicture(self, isHasPicture):
        self._isHasPicture = isHasPicture

    def getIsHasPicture(self):
        return self._isHasPicture

    def setDirectoryRoot(self, directoryRoot):
        self._directoryRoot = directoryRoot
        self._save()

    def getDirectoryRoot(self):
        return self._directoryRoot

    def setDirectory(self, directory):
        self._directory = directory

    def getdirectory(self):
        return self._directory

    def setCurrentDirectoryList(self, currentDirectoryList):
        self._currentDirectoryList = currentDirectoryList
        self._save()

    def getCurrentDirectoryList(self):
        return self._currentDirectoryList

    def setPlaylist(self, playlist):
        self._playlist = playlist
        self._save()

    def getPlaylist(self):
        return self._playlist

    def nextPlaylistmedia(self):
        media = self._playlist.pop()
        self._save()
        return media

    def playlistAppendMedia(self, media):
        self._playlist.append(media)
        self._save()

    # def setShuffleMethodObject(self, shuffleMethodObj):
    #     self._shuffleMethod = shuffleMethodObj

    def getShuffleMethodObject(self):
        return self._shuffleMethod

    def setShuffleMethod(self, shuffleMethod):
        self._shuffleMethod.set(shuffleMethod)
        self._save()

    def getshuffleMethod(self):
        return self._shuffleMethod.get()

    def setMediaPath(self, mediaPath):
        self._mediaPath = mediaPath
        self._save()

    def getMediaPath(self):
        return self._mediaPath

    def appendPreviousMedia(self, media):
        self._previousMedias.append(media)
        if len(self._previousMedias) > 100:
            self._previousMedias.remove(self._previousMedias[0])
        self._save()

    def getPreviousMedia(self):
        media = ''
        if self._previousMedias:
            media = self._previousMedias.pop()
            self._save()
        return media

    def setCustomPlaylist(self, directory):
        self._customPlaylistDirectory = directory
        self._save()

    def addCustomPlaylist(self, playlist, playlistName):
        self._customPlaylistDirectory[playlistName] = playlist
        self._save()

    def getCustomPlaylistDirectory(self):
        return self._customPlaylistDirectory

    def setIsPause(self, isPause):
        self._isPause = isPause

    def getIsPause(self):
        return self._isPause

    def setError(self, error):
        self._error = error
        self._save()

    def getError(self):
        return self._error
