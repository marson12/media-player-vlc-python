import tkinter as tk
from tkinter import ttk
import tkinter.filedialog

import scannerUtil
from guiComponents import PlaylistNamePopup, AddToPlaylistPopUp, ConfiguredFrame, ConfiguredButton, \
    ConfiguredRadiobutton, ConfiguredTree, ConfiguredScrollbar \
    , CreateToolTip


class DirectoryGui(ConfiguredFrame):
    def __init__(self, root, manager, data, *args, **kwargs):
        ConfiguredFrame.__init__(self, root, *args, **kwargs)

        # self.configure(bg=constants.backgroundColor)

        # set connections
        self.manager = manager
        self.data = data

        # add searchers
        searchFrame = ConfiguredFrame(self)
        self.scannerBtn = ConfiguredButton(searchFrame, command=self.findDirectory, text='Choose Folder')
        self.playSong = ConfiguredButton(searchFrame, documentation='Search the file system for media and play that media.',
                                         command=self.playMedia, text='Choose Song')

        # playlist order radio buttons
        radioFrame = ConfiguredFrame(self)

        order_radio = ConfiguredRadiobutton(radioFrame,
                                            text="Order",
                                            variable=self.data.getShuffleMethodObject(), value='Order',
                                            command=self.manager.clearPlayList)

        random_remove_radio = ConfiguredRadiobutton(radioFrame,
                                                    text="Shuffle",
                                                    variable=self.data.getShuffleMethodObject(), value='Shuffle',
                                                    command=self.manager.clearPlayList)
        random_radio = ConfiguredRadiobutton(radioFrame,
                                             text="Random",
                                             variable=self.data.getShuffleMethodObject(), value='Random',
                                             command=self.manager.clearPlayList)

        # playlistButtons
        playlistFrame = ConfiguredFrame(self)
        self.createPlaylistBtn = ConfiguredButton(playlistFrame, text='Create Playlist',
                                                  command=self.createPlaylistPopup)

        self.addMedia = ConfiguredButton(playlistFrame, text='Add to Playlist',
                                         command=self.addToPlaylistPopup)
        self.removePlaylistItem = ConfiguredButton(playlistFrame, text='Remove Playlist',
                                                   command=self.removeCustomPlaylistItem)
        self.playSelectedBtn = ConfiguredButton(playlistFrame, text='Play Selected', command=self.playSelected)

        # create tree frame
        self.treeFrame = ConfiguredFrame(self)
        self.treeScrollBar = ConfiguredScrollbar(self.treeFrame)
        self.directoryTree = ConfiguredTree(self.treeFrame, yscrollcommand=self.treeScrollBar.set)
        self.treeScrollBar.config(command=self.directoryTree.yview)
        self.treeFrame.rowconfigure(0, weight=1)
        self.rowconfigure(3, weight=1)

        # add events
        self.directoryTree.bind('<Double-Button-1>', self.openFileOrFolder)
        self.directoryTree.bind('<Button-3>', self.openWindowsExplorer)

        # PLACE ITEMS
        buttonPadX = buttonPadY = 5
        self.scannerBtn.grid(row=0, column=0, padx=buttonPadX, pady=buttonPadY)
        self.playSong.grid(row=0, column=1, padx=buttonPadX, pady=buttonPadY)
        searchFrame.grid(row=0)

        order_radio.grid(row=0, column=0)
        random_remove_radio.grid(row=0, column=1)
        random_radio.grid(row=0, column=2)
        radioFrame.grid(row=1)

        self.createPlaylistBtn.grid(row=0, column=0, sticky="ew", padx=buttonPadX, pady=buttonPadY)
        self.addMedia.grid(row=0, column=1, sticky="ew", padx=buttonPadX, pady=buttonPadY)

        self.removePlaylistItem.grid(row=1, column=0, sticky="ew", padx=buttonPadX, pady=buttonPadY)
        self.playSelectedBtn.grid(row=1, column=1, sticky="ew", padx=buttonPadX, pady=buttonPadY)
        playlistFrame.grid(row=2)

        self.treeScrollBar.grid(row=0, column=0, sticky="ns")
        self.directoryTree.grid(row=0, column=1, sticky=tk.NSEW)
        self.treeFrame.grid(row=3, sticky=tk.NSEW)

        self.directoryTree.tag_configure('media', background="black")
        self.directoryTree.tag_configure('directory', background="#004466")
        self.directoryTree.tag_configure('custom', background="#24248f")

    def createPlaylistPopup(self):
        if self.directoryTree.selection():
            PlaylistNamePopup(self, self.data.getCustomPlaylistDirectory().keys())

    def addToPlaylistPopup(self):
        if self.directoryTree.selection():
            AddToPlaylistPopUp(self, self.data.getCustomPlaylistDirectory().keys())

    def openWindowsExplorer(self, event):
        item = self.directoryTree.item(self.directoryTree.identify_row(event.y))
        tags = item['tags']

        folderPath = ''

        if tags[1] == 'folder':
            folderPath = tags[0]
        elif tags[1] == 'media':
            folderPath = scannerUtil.getParentDirectory(tags[0])
        scannerUtil.openFolder(folderPath)

    def fillDirectoryTree(self):
        self.data.setDirectory(scannerUtil.createDirectory(self.data.getDirectoryRoot()))
        self.directoryTree.delete(*self.directoryTree.get_children())

        keys = list(self.data.getdirectory().keys())
        keys.reverse()
        for folder in keys:
            folderName = scannerUtil.pathFileName(folder)
            parentPath = scannerUtil.getParentDirectory(folder)
            # add the folder to the tree
            if parentPath == self.data.getDirectoryRoot():
                self.directoryTree.insert('', index='end', iid=folder, text=folderName,
                                          tag=(folder, 'folder', 'directory'))
            elif folder != self.data.getDirectoryRoot():
                self.directoryTree.insert(parent=parentPath, index='end', iid=folder,
                                          text=folderName, tag=(folder, 'folder', 'directory'))

            # add the files the the folder added above
            if folder == self.data.getDirectoryRoot():
                for media in self.data.getdirectory()[folder]:
                    self.directoryTree.insert(parent='', index='end',
                                              text=scannerUtil.pathFileName(media),
                                              tag=(media, 'media'))
            else:
                for media in self.data.getdirectory()[folder]:
                    self.directoryTree.insert(parent=folder, index='end',
                                              text=scannerUtil.pathFileName(media),
                                              tag=(media, 'media'))

    def playMedia(self):
        if self.data.player.is_playing():
            self.manager.pausePlayer()  # opening a dialog while the player is playing will cause an error.
            mediaPath = tk.filedialog.askopenfilename()
        else:
            mediaPath = tk.filedialog.askopenfilename()
        if mediaPath:
            self.manager.play(mediaPath)

    def openFileOrFolder(self, *unusedEvent):
        id = self.directoryTree.selection()[0]
        tags = self.directoryTree.item(id)['tags']
        selectedPath = tags[0]
        itemType = tags[1]

        if itemType == 'media' and len(self.directoryTree.selection()) == 1:
            parent = self.directoryTree.parent(id)
            self.directoryTree.focus()

            if parent:
                folderType = self.directoryTree.item(parent)['tags'][2]
                if folderType == 'directory':
                    self.data.setCurrentDirectoryList(
                        self.data.getdirectory()[self.directoryTree.item(parent)['tags'][0]])
                elif folderType == 'custom':
                    self.data.setCurrentDirectoryList(
                        self.data.getCustomPlaylistDirectory()[self.directoryTree.item(parent)['tags'][0]])
            else:
                self.data.setCurrentDirectoryList(self.data.getdirectory()[self.data.getDirectoryRoot()])
                self.manager.clearPlayList()
            self.manager.play(selectedPath)

        # folder = self.directoryTree.focus()

        elif itemType == 'folder':
            self.directoryTree.get_children(selectedPath)
            medias = self.getMedias(selectedPath)
            self.data.setCurrentDirectoryList(medias)
            self.manager.clearPlayList()
            self.manager.playNext()

    def findDirectory(self):
        folder_path = tk.filedialog.askdirectory()
        if folder_path:
            self.data.setDirectoryRoot(folder_path)
            self.fillDirectoryTree()

    def playSelected(self):
        playlist = []

        for item in self.directoryTree.selection():
            selectedPath = self.directoryTree.item(item)['tags'][0]
            if scannerUtil.isMediaPath(selectedPath):
                if selectedPath not in playlist:
                    playlist.append(selectedPath)
            else:
                playlist.extend(self.getMedias(selectedPath))

        if playlist:
            self.data.setCurrentDirectoryList(playlist)
            self.manager.clearPlayList()
            self.manager.playNext()

    def addCustomTree(self, playlistName):
        playlist = []
        for item in self.directoryTree.selection():
            path = self.directoryTree.item(item)['tags'][0]
            if scannerUtil.isMediaPath(path):
                playlist.append(path)
            else:
                playlist.extend(self.getMedias(path))

        self.data.addCustomPlaylist(playlist, playlistName)
        self.addCustomTreeElement(playlistName, True)
        for media in playlist:
            self.addCustomTreeElement(playlistName, False, media)

    def addCustomTreeElement(self, folderName, isFolder, media=None):
        if isFolder:
            self.directoryTree.insert('', index=0, iid=folderName, text=folderName,
                                      tag=(folderName, 'folder', 'custom'))
        else:
            self.directoryTree.insert(parent=folderName, index='end',
                                      text=scannerUtil.pathFileName(media), tag=(media, 'media'))

    def fillCutstomTree(self):
        for folder in self.data.getCustomPlaylistDirectory().keys():
            self.addCustomTreeElement(folder, True)

            for media in self.data.getCustomPlaylistDirectory()[folder]:
                self.addCustomTreeElement(folder, False, media)

    def addSelectedtoCustomPlaylist(self, folder):
        changeDirectory = False
        if self.data.getCurrentDirectoryList() == self.data.getCustomPlaylistDirectory()[folder]:
            changeDirectory = True

        toAddList = []
        for item in self.directoryTree.selection():
            tags = self.directoryTree.item(item)['tags']
            selectedPath = tags[0]
            mediaType = tags[1]

            if mediaType == 'folder':
                toAddList.extend(self.getMedias(item))
            elif mediaType == 'media':
                toAddList.append(selectedPath)

        for media in toAddList:
            self.data.getCustomPlaylistDirectory()[folder].append(media)
            self.addCustomTreeElement(folder, False, media)

        if changeDirectory:
            self.data.setCurrentDirectoryList(self.data.getCustomPlaylistDirectory()[folder])
            self.manager.clearPlayList()

    def removeCustomPlaylistItem(self):
        id = self.directoryTree.selection()[0]
        tags = self.directoryTree.item(id)['tags']
        path = tags[0]
        itemType = tags[1]
        if itemType == 'folder' and tags[2] == 'custom':
            del self.data.getCustomPlaylistDirectory()[tags[0]]
            self.directoryTree.delete(id)

        elif itemType == 'media':
            parent = self.directoryTree.item(self.directoryTree.parent(id))
            if parent['tags']:
                parentPath = parent['tags'][0]
                if parent['tags'][2] == 'custom':
                    self.data.getCustomPlaylistDirectory()[parentPath].remove(path)
                    self.directoryTree.delete(id)

                    if len(self.data.getCustomPlaylistDirectory()[parentPath]) == 0:
                        del self.data.getCustomPlaylistDirectory()[parentPath]
                        self.directoryTree.delete(parentPath)

    def getMedias(self, path):
        medias = []

        children = self.directoryTree.get_children(path)
        for child in children:

            selectedPath = self.directoryTree.item(child)['tags'][0]

            if scannerUtil.isMediaPath(selectedPath):
                medias.append(selectedPath)
            else:
                medias.extend(self.getMedias(selectedPath))
        return medias

    def fillTree(self):
        self.fillDirectoryTree()
        self.fillCutstomTree()

    def highlightMedia(self, mediaPath):
        self.openItem(mediaPath)
        self.scrollToItem(mediaPath)

    def openItem(self, mediaPath, treeId=None):
        children = self.directoryTree.get_children(treeId)

        # first check for custom playlists
        if not treeId:
            nonCustomChildren = []

            for child in children:
                treeItem = self.directoryTree.item(child)
                if treeItem['tags'][1] == 'folder':
                    if treeItem['tags'][2] == 'custom':
                        if self.data.getCustomPlaylistDirectory()[child] == self.data.getCurrentDirectoryList():
                            self.openItem(mediaPath, child)
                            self.directoryTree.item(child, open=True)
                            return
                        else:
                            nonCustomChildren.append(child)
                    else:
                        nonCustomChildren.append(child)
            children = nonCustomChildren

        # then check for the music and the folders

        for child in children:
            treeItem = self.directoryTree.item(child)

            if treeItem['tags'][1] == 'folder':
                if treeItem['tags'][0] + '/' in mediaPath:
                    self.openItem(mediaPath, child)
                    self.directoryTree.item(child, open=True)
                    return
            elif treeItem['tags'][1] == 'media':
                if mediaPath == treeItem['tags'][0]:
                    self.directoryTree.focus(child)
                    self.directoryTree.selection_set(child)
                    return

    def scrollToItem(self, mediaPath):
        scrollInformation = {'itemCount': 0, 'mediaPosition': None}

        def dig(treeId):
            scrollInformation['itemCount'] += 1
            treeItem = self.directoryTree.item(treeId)
            if treeItem['tags'][1] == 'folder' and treeItem['open']:
                for child in self.directoryTree.get_children(treeId):
                    dig(child)
            elif treeItem['tags'][1] == 'media':
                if treeItem['tags'][0] == mediaPath:
                    scrollInformation['mediaPosition'] = scrollInformation['itemCount']

        for child in self.directoryTree.get_children():
            dig(child)
        try:

            # subtract 1 so that the media is at the top instead of just above the top
            position = (scrollInformation['mediaPosition'] - 1) / scrollInformation['itemCount']
            # self.directoryTree.yview_moveto(.96825)
            self.directoryTree.yview_moveto(position)
            # self.directoryTree.yview('moveto', position)
        except:
            pass
