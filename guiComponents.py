import time

import tkinter as tk
from tkinter import ttk

import constants


class PlaylistNamePopup():
    def __init__(self, creater, playlistNames):
        self.creater = creater

        self.playlistNames = playlistNames

        top = self.top = tk.Toplevel(creater)
        self.setNameLabel = tk.Label(top, text="Set playlistName")
        self.setNameLabel.grid()
        self.nameEntry = tk.Entry(top)
        self.nameEntry.grid()
        self.createBtn = ConfiguredButton(top, text='Set Name', command=self.setName)
        self.cancelBtn = ConfiguredButton(top, text='Cancel', command=self.cancel)
        self.createBtn.grid()
        self.cancelBtn.grid()

    def setName(self):
        playlistName = self.nameEntry.get()
        if playlistName and playlistName in self.playlistNames:
            self.setNameLabel.configure(foreground="red", text='NAME ALREADY USED')
        else:
            self.creater.addCustomTree(playlistName)
            self.top.destroy()

    def cancel(self):
        self.top.destroy()


class AddToPlaylistPopUp():
    def __init__(self, creater, folderNames):
        self.creater = creater

        self.folderNames = folderNames
        top = self.top = tk.Toplevel(creater)

        self.messageLabel = tk.Label(top, text='Doubleclick the folder to add to')
        self.messageLabel.grid()

        #
        # self.treeScrollBar.config(command=self.directoryTree.yview)
        # self.directoryTree.grid(row=0, column=1, sticky=tk.NSEW)
        # self.treeFrame.grid(row=3, sticky=tk.NSEW)
        #
        # self.treeFrame.rowconfigure(0, weight=1)
        # self.rowconfigure(3, weight=1)
        #

        self.treeFrame = tk.Frame(top)
        self.treeScrollBar = tk.Scrollbar(self.treeFrame)
        self.treeScrollBar.grid(row=0, column=0, sticky="ns")

        self.folderTree = ttk.Treeview(self.treeFrame, yscrollcommand=self.treeScrollBar.set)

        self.treeScrollBar.config(command=self.folderTree.yview)
        self.folderTree.grid(row=0, column=1, sticky=tk.NSEW)

        for folder in self.folderNames:
            self.folderTree.insert(parent='', index='end', iid=folder, text=folder)
        self.folderTree.bind('<Double-Button-1>',
                             lambda x: self.creater.addSelectedtoCustomPlaylist(self.folderTree.selection()[0]))
        self.treeFrame.grid()
        self.cancelBtn = tk.Button(top, text='Cancel', command=self.cancel)
        # self.createBtn.grid()
        self.cancelBtn.grid()

        self.folderTree.heading("#0", text="Custom Folders")

    def cancel(self):
        self.top.destroy()


class CreateToolTip(object):
    '''
    create a tooltip for a given widget
    '''

    def __init__(self, widget, documentation='widget info'):
        self.widget = widget
        self.documentation = documentation
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.close)

    def enter(self, event=None):
        if not self.documentation:
            return
        time.sleep(.5)
        x = y = 0

        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = tk.Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = tk.Label(self.tw, text=self.documentation, justify='left',
                         background='lightblue', relief='solid', borderwidth=1,
                         font=("times", "8", "normal"))
        # label = tk.Label(self.tw, text=self.message)
        label.grid()

    def close(self, event=None):
        if self.tw:
            self.tw.destroy()

    def grid(self, *args, **kwargs):
        self.widget.grid(*args, **kwargs)

    def bind(self, *args, **kwargs):
        self.widget.bind(*args, **kwargs)
    def changeWidgetText(self, text):
        self.widget['text'] = text


class ConfiguredLabel(tk.Label):
    def __init__(self, root, *args, **kwargs):
        tk.Label.__init__(self, root, *args, **kwargs)
        self.configure(bg=constants.backgroundColor,
                       foreground=constants.foreground)


class ConfiguredFrame(tk.Frame):
    def __init__(self, root, **kwargs):
        tk.Frame.__init__(self, root, **kwargs)
        self.configure(bg=constants.backgroundColor)


# class ConfiguredButton(tk.Button):
#     def __init__(self, root, *args, **kwargs):
#         tk.Button.__init__(self, root, *args, **kwargs)
#         self.configure(bg=constants.componentBackground, foreground=constants.foreground)


class ConfiguredButton(CreateToolTip):
    def __init__(self, root,documentation='', **kwargs):
        button = tk.Button(root, **kwargs)
        button.configure(bg=constants.componentBackground, foreground=constants.foreground)

        CreateToolTip.__init__(self, button,documentation)


class ConfiguredRadiobutton(tk.Radiobutton):
    def __init__(self, root, *args, **kwargs):
        tk.Radiobutton.__init__(self, root, *args, **kwargs)
        self.configure(bg=constants.backgroundColor, selectcolor=constants.componentBackground,
                       foreground=constants.foreground)


class ConfiguredCheckBox(tk.Checkbutton):
    def __init__(self, root, *args, **kwargs):
        tk.Checkbutton.__init__(self, root, *args, **kwargs)
        self.configure(bg=constants.backgroundColor, selectcolor=constants.componentBackground,
                       foreground=constants.foreground)


class ConfiguredTree(ttk.Treeview):
    def __init__(self, root, **kwargs):
        ttk.Treeview.__init__(self, root, **kwargs)

        style = ttk.Style(root)
        style.theme_use('winnative')
        # style.theme_use('clam')
        # style.theme_use('alt')
        # style.theme_use('default')
        # style.theme_use('classic')

        style.configure("Treeview",
                        fieldbackground=constants.backgroundColor, foreground='silver', background="#D3D3D3",
                        relief="SUNKEN"
                        )
        self.heading("#0", text="Directory")
        style.map('Treeview',
                  background=[('selected', 'darkblue')])

        style.map('Treeview', foreground=self.fixedMap("foreground", style))

        ttk.Style().configure("Treeview.Heading", background=constants.backgroundColor, foreground=constants.foreground)

        style.element_create("Custom.Treeheading.border", "from", "default")
        style.layout("Custom.Treeview.Heading", [
            ("Custom.Treeheading.cell", {'sticky': 'nswe'}),
            ("Custom.Treeheading.border", {'sticky': 'nswe', 'children': [
                ("Custom.Treeheading.padding", {'sticky': 'nswe', 'children': [
                    ("Custom.Treeheading.image", {'side': 'right', 'sticky': ''}),
                    ("Custom.Treeheading.text", {'sticky': 'we'})
                ]})
            ]}),
        ])
        style.configure("Custom.Treeview.Heading",
                        background="blue", foreground="white", relief="flat")
        style.map("Custom.Treeview.Heading",
                  relief=[('active', 'groove'), ('pressed', 'sunken')])

    def fixedMap(self, option, style):
        # foreground didnt work for me on this version
        return [elm for elm in style.map("Treeview", query_opt=option)
                if elm[:2] != ("!disabled", "!selected")]


class ConfiguredScrollbar(tk.Scrollbar):
    def __init__(self, root, **kwargs):
        tk.Scrollbar.__init__(self, root, **kwargs)


class ConfiguredScale(ttk.Scale):
    def __init__(self, root, **kwargs):
        style = ttk.Style()
        style.configure("Horizontal.TScale", background=constants.componentBackground, fieldbackground='green')

        ttk.Scale.__init__(self, root, style="Horizontal.TScale", **kwargs)

    def fixedMap(self, option, style):
        # foreground didnt work for me on this version
        return [elm for elm in style.map("Treeview", query_opt=option)
                if elm[:2] != ("!disabled", "!selected")]

# if __name__ == '__main__':
#     root = tk.Tk()
#     root.geometry('300x300')
#     scale = ConfiguredScale(root)
#     scale.pack()
#
#     root.mainloop()
