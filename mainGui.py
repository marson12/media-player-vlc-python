import threading
import tkinter as tk

from pynput.keyboard import Listener

from data import Data
from controlsGui import ControlsGui
from mediaManager import MediaManager
from directoryGui import DirectoryGui
from mediaPlayerGui import MediaPlayerGui


class MainGui(tk.Frame):
    # def __init__(self, root, *args, **kwargs):
    def __init__(self):
        self.root = tk.Tk()

        # init parent class
        tk.Frame.__init__(self, self.root)

        # set connection
        self.data = Data(self)
        self.manager = MediaManager(self.data, self)

        # config root
        self.master.minsize(200, 200)
        self.master.geometry('950x950')


        

        # create subFrames
        self.rightFrame = tk.Frame(self)  # contains both vlcplayer and mediacontrollergui. DirectoryGui IS LEFT FRAME
        self.mediaPlayerGui = MediaPlayerGui(self.rightFrame, self.manager, self.data)
        self.DirectoryGui = DirectoryGui(self, self.manager, self.data)
        self.controlsGui = ControlsGui(self.rightFrame, self.manager, self.data)

        # position subframes
        self.mediaPlayerGui.grid(row=0, column=0, sticky=tk.N + tk.S + tk.E + tk.W)
        self.setNonvideoFrame()
        self.rightFrame.grid(row=0, column=1, sticky=tk.N + tk.S + tk.E + tk.W)

        self.rightFrame.columnconfigure(0, weight=1)
        self.rightFrame.rowconfigure(0, weight=1)

        self.columnconfigure(1, weight=1)
        self.rowconfigure(0, weight=1)

        # pack self
        self.grid()
        # events
        self.addEvents()

        # this is so that the timer will make sure that the mouse hasnt moved in so much time
        self.mouseLocation = (0, 0)

        self.pack(fill='both', expand=True)

        self.manager.setSubComponents(self.DirectoryGui, self.mediaPlayerGui, self.controlsGui)
        self.manager.loadDataToPlayer()

        self.root.protocol("WM_DELETE_WINDOW", self.destroy)

    def destroy(self):
        self.mediaPlayerGui.destroy()
        super().destroy()
        self.root.destroy()

        # self.mediaPlayerGui.destroy()
        # self.root.destroy()
        # super().destroy()

    def setNonvideoFrame(self):
        self.DirectoryGui.grid(row=0, column=0, sticky=tk.N + tk.S + tk.E + tk.W)
        self.controlsGui.grid(row=1, column=0, sticky=tk.N + tk.S + tk.E + tk.W)

    def addEvents(self):
        # tkinter keys
        self.master.bind('<Escape>', self.deactivateFullScreen)
        self.master.bind('<Motion>', self.motionTimer)

        self.master.bind('<Key>', self.shortCuts)
        # self.master.bind('<Up>', lambda x: self.data.setVolume(self.data.getVolume() + 5))
        # self.master.bind('<Down>', lambda x: self.data.setVolume(self.data.getVolume() - 5))
        self.master.bind('<Left>', lambda x: self.manager.changeTime(-5000))
        self.master.bind('<Right>', lambda x: self.manager.changeTime(5000))
        self.master.bind('<Shift-N>', self.manager.playNext)
        self.mediaPlayerGui.bind('<Key>', self.shortCuts)

        #     media events.
        #  these events can be used even when the application is not focused by windows.
        Listener(on_press=self.mediaKeyEvents).start()

    def motionTimer(self, event):
        self.setNonvideoFrame()
        self.master.overrideredirect(False)

        y = event.y
        x = event.x
        # timer = threading.Timer(2, lambda: self.onLeave() if x == self.mouseLocation[0] and y == self.mouseLocation[
        timer = threading.Timer(15, lambda: self.onLeave() if x == self.mouseLocation[0] and y == self.mouseLocation[
            1] else None)

        timer.start()
        self.mouseLocation = (x, y)

    def onLeave(self, *unusedEvent):
        self.DirectoryGui.grid_forget()
        self.controlsGui.grid_forget()

        # check if the screen should be maximized.
        screen_w, screen_h = self.master.winfo_screenwidth(), self.master.winfo_screenheight()
        window_w, window_h = self.master.winfo_width(), self.master.winfo_height()

        # 100 is the the windows toolbar. screen is allowed to be just a little shorter.
        if screen_w == window_w and screen_h > window_h > screen_h - 100:
            self.activateFullScreen()

    def activateFullScreen(self, *unuserEvent):
        self.data.setIsFullScreen(True)
        self.master.overrideredirect(True)
        self.master.state("zoomed")
        self.controlsGui.setFullScreen()

    def deactivateFullScreen(self, *unuserEvent):
        self.data.setIsFullScreen(False)
        self.master.state("normal")
        self.master.geometry("960x540")
        self.master.overrideredirect(False)
        self.controlsGui.setMinimizeScreen()

    def shortCuts(self, event):
        command = event.char

        if command == ' ':
            self.manager.togglePlay()
            pass
        elif command == 'f':
            self.manager.toggleFullscreen()
        elif command == 'm':
            self.manager.toggleMute()

        # control location percent
        elif command == '0':
            self.manager.setTimePercent(0)
        elif command == '1':
            self.manager.setTimePercent(10)
        elif command == '2':
            self.manager.setTimePercent(20)
        elif command == '3':
            self.manager.setTimePercent(30)
        elif command == '4':
            self.manager.setTimePercent(40)
        elif command == '5':
            self.manager.setTimePercent(50)
        elif command == '6':
            self.manager.setTimePercent(60)
        elif command == '7':
            self.manager.setTimePercent(70)
        elif command == '8':
            self.manager.setTimePercent(80)
        elif command == '9':
            self.manager.setTimePercent(90)

    def mediaKeyEvents(self, key):
        try:
            if key.name == 'media_volume_mute':
                self.manager.toggleMute()
            elif key.name == 'media_play_pause':
                self.manager.toggleMute()
            elif key.name == 'media_next':
                self.manager.playNext()
            elif key.name == 'media_previous':
                self.manager.playPrevious()
        except:
            pass  # if the key is none of these then it doesnt matter.

    def setGuiTitle(self, text):
        self.master.title(text)
