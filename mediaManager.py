import random
import time

import scannerUtil

class MediaManager:
    def __init__(self, data, gui):
        self.data = data
        self.gui = gui

    def setSubComponents(self, DirectoryGui, mediaPlayerGui, controlsGui):
        self.DirectoryGui = DirectoryGui
        self.mediaPlayerGui = mediaPlayerGui
        self.controlsGui = controlsGui

    def loadDataToPlayer(self):
        self.data.getVolumeObject().trace('w', self.setVolume)

        self.DirectoryGui.fillTree()

        if self.data.getMediaPath():
            self.play(self.data.getMediaPath())

            time.sleep(.1)  # Nobody is as mad about this as I am. but it looks like it takes a moment for the
            # media player to set some stuff up in the background I think???
            # Either way If I dont give some padding I cannt change the time to the previous moment.
            #  I think I can set the sleep time to .01301 seconds, but that might be dependent on the computer,
            #  so I have it set to .1 seconds. I think that should be slow enough where it doesnt matter.
            self.changeTime()

        self.setVolume()

    def toggleMute(self):
        if self.data.getIsMuted():
            self.data.player.audio_set_mute(False)
        else:
            self.data.player.audio_set_mute(True)
        self.data.setIsMuted(not self.data.getIsMuted())

    def mediaTimeChanged(self, *unusedEvent):
        self.data.setTime(self.data.player.get_position() * 100000)
        timeWatched = self.millisecondsToString(self.data.player.get_time())
        totalTime = self.millisecondsToString(self.data.player.get_length())
        self.controlsGui.setWatchedLabel(timeWatched + ' / ' + totalTime)

    def millisecondsToString(self, milliseconds):
        timeText = ''
        # hours
        if (milliseconds / (1000 * 60 * 60)) % 24 > 1:
            timeText += str(int((milliseconds / (1000 * 60 * 60)) % 24)) + ':'

        # minutes
        if (milliseconds / (1000 * 60)) % 60 > 1:
            timeText += str(int((milliseconds / (1000 * 60)) % 60)) + ':'
        else:
            timeText += '0:'

        # seconds
        if (milliseconds / 1000) % 60 < 10:
            timeText += '0'
        timeText += str(int((milliseconds / 1000) % 60))
        return timeText

    def playNext(self, *unusedEvent):
        if self.data.getCurrentDirectoryList():
            if self.data.getPlaylist():
                self.play(self.data.nextPlaylistmedia())
            else:
                if self.data.getshuffleMethod() == 'Order':
                    #             in this method the list is in order from the point after the media was watched.
                    #  example, [a,b,c,d,e], user just watched c. playlist will be [d,e,a,b]

                    playlist = []

                    if self.data.getMediaPath() in self.data.getCurrentDirectoryList():
                        # case of the user has already selected a song in the playlsit.
                        index = self.data.getCurrentDirectoryList().index(self.data.getMediaPath())
                        # for media in self.data.getCurrentDirectoryList():
                        #     count += 1
                        #     if media == self.data.getMediaPath():
                        #         break

                        playlist = self.data.getCurrentDirectoryList()[index + 1:]  # +1 excludes the current song
                        playlist.extend(self.data.getCurrentDirectoryList()[:index])

                    else:
                        # case of a new order and want to start from the beginning.
                        playlist = [*self.data.getCurrentDirectoryList()]


                    self.data.setPlaylist(playlist)

                elif self.data.getshuffleMethod() == 'Shuffle':
                    # will collect all of the songs in the folder, and then shuffle the order
                    # og: [a,b,c,d,e], ->playnext style=shuffle -> [d,a,c,b,e]
                    playlist = [*self.data.getCurrentDirectoryList()]
                    random.shuffle(playlist)
                    self.data.setPlaylist(playlist)

                elif self.data.getshuffleMethod() == 'Random':
                    # will randomly select a media from the folder. songs twice in a row can happen.
                    self.data.setPlaylist([random.choice(self.data.getCurrentDirectoryList())])

                if self.data.getPlaylist():  # case of there being only one file in the folder
                    # self.mediaGui.addMedia(self.data.playlist.pop())
                    # self.singleSong(self.data.playlist.pop())
                    self.play(self.data.nextPlaylistmedia())

    def playPrevious(self):
        #     takes and plays the previous song and plays that. puts the current song into data.playlist

        self.data.playlistAppendMedia(self.data.getMediaPath())
        previous = self.data.getPreviousMedia()
        self.data.setMediaPath(self.data.getPreviousMedia())
        self.play(previous)

    def changeTime(self, changeAmount=0, *unusedEvents):
        setTime = self.data.getTime() / 100000

        try:
            change = int(changeAmount)
            setTime += change / self.data.player.get_length()
        except:
            # what was passed in was an event not an integer
            pass

        self.data.player.set_position(setTime)
        self.mediaTimeChanged()

    def setTimePercent(self, percent):
        self.data.player.set_position(percent / 100)
        self.mediaTimeChanged()

    def togglePlay(self, *unusedEvent):
        if self.data.player.is_playing():
            self.pausePlayer()
        else:
            self.unPausePlayer()

    def pausePlayer(self):
        self.data.player.set_pause(9001)
        self.controlsGui.setUnPauseButton()

    def unPausePlayer(self):
        self.data.player.set_pause(0)
        self.controlsGui.setPauseButton()

    def mediaEnd(self, *unusedEvent):
        if self.data.getIsRepeat():
            self.play(self.data.getMediaPath())

        elif self.data.getMediaPath():
            self.playNext()

    def play(self, mediaPath):
        try:
            if self.data.getMediaPath() and self.data.getMediaPath() != mediaPath:
                self.data.appendPreviousMedia(self.data.getMediaPath())
            self.data.setMediaPath(mediaPath)

            self.gui.setGuiTitle(scannerUtil.pathFileName(mediaPath))

            self.mediaPlayerGui.play()
            self.controlsGui.setMusicControls()
            if self.data.getCurrentDirectoryList():
                self.DirectoryGui.highlightMedia(mediaPath)
        except Exception as error:
            self.data.setError(str(error))

    def setVolume(self, *unusedEvent):
        volume = self.data.getVolume()
        if volume < 0:
            volume = 0
            self.data.setVolume(0)
        elif 100 < volume:
            volume = 100
            self.data.setVolume(100)

        self.controlsGui.setVolumeLabel(volume)
        self.data.player.audio_set_volume(volume)

    def clearPlayList(self):
        self.data.setPlaylist([])

    def toggleFullscreen(self):
        if self.data.getIsFullScreen():
            self.gui.deactivateFullScreen()
        else:
            self.gui.activateFullScreen()

    def toggleWallpaper(self):
        self.mediaPlayerGui.chooseDisplay()
        self.controlsGui.setMusicControls()
