import time
import tkinter as tk

import vlc

import scannerUtil
from mp3Frame import Mp3Frame


class MediaPlayerGui(tk.Frame):
    def __init__(self, root, manager, data, *args, **kwargs):
        tk.Frame.__init__(self, root, *args, **kwargs)

        # set connection
        self.manager = manager
        self.data = data

        # vlc
        self.Instance = vlc.Instance()

        self.media_player = vlc.MediaListPlayer()
        self.createPictureFrame()
        self.createVideoFrame()

        self.grid(row=0, column=0, sticky=tk.N + tk.S + tk.E + tk.W)

        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)

    def play(self):
        media = self.Instance.media_new(self.data.getMediaPath())
        self.media_list.add_media(media)

        self.chooseDisplay()

        state = str(self.data.player.get_state())

        if state in ('State.Playing', 'State.Paused'):
            self.media_player.next()
            time.sleep(2)  # vlc media player likes to bug out if media_player.next() is pressed to many times
            # in succession. this helps prevent that bug from happening. need to find a better solution.

        elif state in ('State.NothingSpecial', 'State.Ended'):
            # for first play, or for after a video is done.
            self.media_player.play()

    def chooseDisplay(self):
        if scannerUtil.isMusicPath(self.data.getMediaPath()) or self.data.getIsUsewallpaper():
            if self.data.getIsUsewallpaper():
                self.mp3Frame.useDesktopWallpaper()
                self.data.setIsHasPicture(True)
            else:
                self.data.setIsHasPicture(self.mp3Frame.musicHasPicture(self.data.getMediaPath()))
                if self.data.getIsHasPicture():
                    self.mp3Frame.useMp3Picture()
                else:
                    self.mp3Frame.useDesktopWallpaper()
            self.mp3Frame.tkraise()
        else:
            self.videoFrame.tkraise()

    def createVideoFrame(self):
        self.videoFrame = tk.Frame(self, background='black')

        self.videoFrame.columnconfigure(0, weight=1)
        self.videoFrame.rowconfigure(0, weight=1)

        self.videoFrame.grid(sticky=tk.N + tk.S + tk.E + tk.W)

        self.media_list = self.Instance.media_list_new()
        self.media_player.set_media_list(self.media_list)

        self.data.player = self.media_player.get_media_player()
        self.data.player.set_hwnd(self.videoFrame.winfo_id())

        vlc.libvlc_video_set_mouse_input(self.data.player, False)
        vlc.libvlc_video_set_key_input(self.data.player, False)

        self.data.player.event_manager().event_attach(vlc.EventType.MediaPlayerEndReached, self.manager.mediaEnd)

        self.data.player.event_manager().event_attach(vlc.EventType.MediaPlayerTimeChanged,
                                                      self.manager.mediaTimeChanged)

        self.videoFrame.bind('<Button-1>', self.manager.togglePlay)
        self.videoFrame.grid(row=0, column=0, sticky=tk.N + tk.S + tk.E + tk.W)

    def createPictureFrame(self):
        self.mp3Frame = Mp3Frame(self, self.data)
        self.mp3Frame.bind('<Button-1>', self.manager.togglePlay)
        self.mp3Frame.grid(row=0, column=0, sticky=tk.N + tk.S + tk.E + tk.W)

    def destroy(self):
        self.mp3Frame.destroy()
        super().destroy()
