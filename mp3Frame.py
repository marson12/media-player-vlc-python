import threading

import tkinter as tk

from PIL import ImageTk, Image
import eyed3

import constants
import scannerUtil


class Mp3Frame(tk.Label):
    def __init__(self, master, data, *args, **kwargs):
        tk.Label.__init__(self, master, *args, **kwargs)

        # state object
        self.data = data

        self.config(background='black')
        self.grid(sticky=tk.N + tk.S + tk.E + tk.W)
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)
        self.bind('<Configure>', self.onResize)

        self.origionalImage = None

        self.isWallpaperTimer = False
        self.wallpaperStats = {}

    def useDesktopWallpaper(self):
        self.isWallpaperTimer = True
        self.startSetWallpaper()

    def startSetWallpaper(self):
        currentStats = scannerUtil.getStats(constants.wallpaperLocation)
        # if not self.data.isPause:

        if self.isWallpaperTimer:
            if self.wallpaperStats != currentStats and not self.data.getIsPause():
                self.wallpaperStats = currentStats
                try:  # can error out if the image is being changed right when its grabbed
                    scannerUtil.copyFile(constants.wallpaperLocation, constants.currentImageLocation)
                    self.setImage(constants.currentImageLocation)
                except:
                    pass
            timer = threading.Timer(1, self.startSetWallpaper)
            timer.start()

    def useMp3Picture(self):
        print('USING IMAGE PICTURE')
        audiofile = eyed3.load(self.data.getMediaPath())
        self.stopWallpaperScan()

        eyed3Image = audiofile.tag.images[0]
        image_file = open(constants.currentImageLocation, "wb")
        image_file.write(eyed3Image.image_data)
        image_file.close()

        self.setImage(constants.currentImageLocation)

    def setImage(self, imagePath):
        self.origionalImage = Image.open(imagePath)
        self.imageHeigtWidthRatio = self.origionalImage.height / self.origionalImage.width

        self.background_image = ImageTk.PhotoImage(self.origionalImage)
        self.configure(image=self.background_image)
        # scannerUtil.deleteFile(imagePath)

        try:
            self.onResize()  # this breaks on opening the program, as the play button starts before the window is opened
        #    consider a different  way. perhaps after it is opened somehow
        except:
            pass

    def onResize(self, *unusedEvent):
        if not self.origionalImage or self.winfo_height() == 0 or self.winfo_width() == 0:
            return

        frameRatio = self.winfo_height() / self.winfo_width()

        if self.imageHeigtWidthRatio < frameRatio:
            imageWidth = self.winfo_width()
            imageHeight = self.winfo_width() * self.imageHeigtWidthRatio
        else:
            imageWidth = self.winfo_height() / self.imageHeigtWidthRatio
            imageHeight = self.winfo_height()

        imageWidth = int(imageWidth)
        imageHeight = int(imageHeight)

        self.newImage = self.origionalImage.resize((imageWidth, imageHeight), Image.ANTIALIAS)
        self.background_image = ImageTk.PhotoImage(self.newImage)
        self.configure(image=self.background_image)

    def stopWallpaperScan(self):
        self.isWallpaperTimer = False
        self.wallpaperStats = {}

    def grid_forget(self):
        self.stopWallpaperScan()
        super().grid_forget()

    def pack_forget(self):
        self.stopWallpaperScan()
        super().pack_forget()

    @staticmethod
    def musicHasPicture(mediaPath):
        audioFile = eyed3.load(mediaPath)
        if audioFile.tag and audioFile.tag.images:
            print('hAS PICTURE')
            return True
        print('NO PICTURE')
        return False

        # if not audioFile.tag or not audioFile.tag.images:
        #     return False
    def destroy(self):
        self.stopWallpaperScan()
        super().destroy()