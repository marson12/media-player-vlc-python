import os
import shutil
import sys
from pathlib import Path

import constants


def createDirectory(topPath):
    directory = {}
    if topPath:
        directory[topPath], foundSongs = scanDirectory(directory, topPath)
    return sortFiles(directory)


def scanDirectory(directory, path):
    mediaList = []
    hasMedia = False  # this is to make sure that containing folders are included
    for entry in os.listdir(path):
        fullPath = path + '/' + entry
        if os.path.isdir(fullPath):
            scannedList, foundMedia = scanDirectory(directory, fullPath)
            if foundMedia:  # dont add empty directories.
                directory[fullPath] = scannedList
                hasMedia = foundMedia

        elif isMediaPath(fullPath):
            mediaList.append(fullPath)
            hasMedia = True
    return mediaList, hasMedia


def sortFiles(directory):
    for key in directory.keys():
        directory[key] = sorted(directory[key])
    return directory


def getParentDirectory(directory):
    if '/' in directory:
        slashCharacter = '/'
    elif '\\' in directory:
        slashCharacter = '\\'

    split = directory.split(slashCharacter)

    parent = ''
    for dir in split[:-1]:
        if parent:
            parent += slashCharacter
        parent += dir
    return parent


def getSuffix(path):
    return path.split(".")[-1].strip().lower()


def isMediaPath(path):
    return getSuffix(path) in constants.mediaTypes


def isVideoPath(path):
    return getSuffix(path) in constants.videoTypes


def isMusicPath(path):
    return getSuffix(path) in constants.musicTypes


def isDirectory(path):
    pass


def isPicture(path):
    return path.split(".")[-1].strip().lower() in constants.pictureTypes


def pathFileName(path):
    return path.split("/")[-1].strip()


def getDataFilePath():
    return sys.argv[0] + '_save.ini'


def deleteDataFile():
    if fileExists(getDataFilePath()):
        os.remove(getDataFilePath())


def deleteFile(path):
    if fileExists(path):
        os.remove(path)


def fileExists(path):
    return os.path.exists(path)


def programArguments():
    if len(sys.argv) > 1:
        arguments = ''
        for string in sys.argv[1:]:
            if arguments:
                arguments += ' '
            arguments += string
        return arguments
    else:
        return ''


def openFolder(folderPath):
    os.startfile(folderPath)


def copyFile(path, destination=None):
    if not destination:
        destination = pathFileName(path) + ' ' + getSuffix(path)
    shutil.copy(path, destination)


def getStats(path):
    return Path(path).stat()
